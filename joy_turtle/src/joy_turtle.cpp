#include <ros/ros.h>
//#include <visualization_msgs/Marker.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>

class SubscribeAndPublish
{
public:
    SubscribeAndPublish()
{
    //Topic you want to publish
    pub_ = n_.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1);

    //Topic you want to subscribe
    sub_ = n_.subscribe("/joy", 1, &SubscribeAndPublish::callback, this);
}

void callback(const sensor_msgs::Joy joy_input)
{
    geometry_msgs::Twist geom_msg;

    geom_msg.linear.x  = joy_input.axes[1];
    geom_msg.angular.z = joy_input.axes[0];

    pub_.publish(geom_msg);
  }

private:
    ros::NodeHandle n_; 
    ros::Publisher pub_;
    ros::Subscriber sub_;

};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscribe_and_publish");
    SubscribeAndPublish SAPObject;
    ros::spin();
    return 0;
}
